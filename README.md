# docker-examples

## Repo with docker examples
* `aws-sns` - Running AWS SNS, SQS in docker
* `aws-kinesis` - Running Kinesis in docker
* `kong-api-gw` - Running Kong Api GW with Konga dashboard in docker