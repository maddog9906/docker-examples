package com.battletech.maddog.docker.example.kinesis.helper

import com.amazonaws.ClientConfiguration
import com.amazonaws.Protocol
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.kinesis.AmazonKinesis
import com.amazonaws.services.kinesis.AmazonKinesisClientBuilder
import com.amazonaws.services.kinesis.model.*

class KinesisHelper {

    static AmazonKinesis getKinesisClient(String serviceEndpoint) {
        URL url = new URL(serviceEndpoint)
        if (!(url.protocol in ["http","https"])) {
            throw new RuntimeException("Only HTTP or HTTPS protocol is supported for Kinesis!!!!")
        }

        AmazonKinesisClientBuilder.standard()
            .withCredentials(AWSHelper.AWS_DUMMY_CREDENTIALS)
            .withClientConfiguration(new ClientConfiguration(protocol: url.protocol=="https"? Protocol.HTTPS : Protocol.HTTP))
            .withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("${url.host}:${url.port}", AWSHelper.AWS_REGION)
            ).build()
    }

    static boolean isStreamAvailable(AmazonKinesis client, String streamName) {
        ListStreamsRequest listStreamsRequest = new ListStreamsRequest()
        listStreamsRequest.setLimit(100)
        ListStreamsResult listStreamsResult = client.listStreams(listStreamsRequest)
        listStreamsResult.getStreamNames().find{ it == streamName }
    }

    static void waitForStream(AmazonKinesis client, String streamName) {
        DescribeStreamRequest describeStreamRequest = new DescribeStreamRequest()
        describeStreamRequest.setStreamName(streamName)

        long startTime = System.currentTimeMillis()
        long endTime = startTime + ( 2 * 60 * 1000 )
        while ( System.currentTimeMillis() < endTime ) {
            try {
                DescribeStreamResult describeStreamResponse = client.describeStream(describeStreamRequest)
                String streamStatus = describeStreamResponse.getStreamDescription().getStreamStatus()
                if ( streamStatus=="ACTIVE") {
                    return
                }
                Thread.sleep( 1_000 )
            }
            catch ( ResourceNotFoundException e ) {}
        }
        throw new RuntimeException( "Stream " + streamName + " never went active" )
    }
}
