package com.battletech.maddog.docker.example.kinesis.helper

import com.amazonaws.services.kinesis.producer.KinesisProducer
import com.amazonaws.services.kinesis.producer.KinesisProducerConfiguration

class KinesisProducerHelper {
    static KinesisProducer getKinesisProducer(String serviceEndpoint) {
        URL url = new URL(serviceEndpoint)
        if (url.protocol!="https") {
            throw new RuntimeException("Only HTTPS protocol is supported for KPL!!!!")
        }

        KinesisProducerConfiguration config =
                new KinesisProducerConfiguration(
                        credentialsProvider: AWSHelper.AWS_DUMMY_CREDENTIALS,
                        kinesisEndpoint: url.host,
                        kinesisPort: url.port,
                        region: AWSHelper.AWS_REGION,
                        verifyCertificate: false,
                        maxConnections: 1
                )
        new KinesisProducer(config)
    }
}
