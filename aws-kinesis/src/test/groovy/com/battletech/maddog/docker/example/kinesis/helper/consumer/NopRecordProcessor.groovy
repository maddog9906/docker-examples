package com.battletech.maddog.docker.example.kinesis.helper.consumer

import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.ShutdownReason
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownInput

class NopRecordProcessor implements IRecordProcessor {
    String applicationName
    String streamName
    long recordsCount = 0
    long expectedRecordsCount
    Closure shutdownClosure = null
    int maxExecutionTime = 5_000
    TimerTask timerTask

    NopRecordProcessor(String applicationName, String streamName, long expectedRecordsCount) {
        this.applicationName = applicationName
        this.streamName = streamName
        this.expectedRecordsCount = expectedRecordsCount
    }

    private NopRecordProcessor(){}

    @Override
    void initialize(InitializationInput initializationInput) {
        println("RecordProcessor initialized: [stream=${streamName}, shardId=${initializationInput.shardId}]")
        def timer = new Timer()
        timerTask = timer.runAfter(maxExecutionTime){
            println("Max wait time reached.... forcing shutdown....")
            shutdownClosure()
        }
    }

    @Override
    void processRecords(ProcessRecordsInput processRecordsInput) {
        println("Received [${processRecordsInput.records.size()}] records on stream [${streamName}]")
        recordsCount += processRecordsInput.records.size()

        processRecordsInput.records.each { record ->
            String message = new String(record.getData().array(), "UTF-8")
            println("Stream [${streamName}], Seq. No [${record.sequenceNumber}], data [${message}]")
        }

        processRecordsInput.checkpointer.checkpoint()

        if (recordsCount == expectedRecordsCount) {
            shutdownClosure()
        }
    }

    @Override
    void shutdown(ShutdownInput shutdownInput) {
        println("RecordProcessor shutdown")

        if (shutdownInput.shutdownReason == ShutdownReason.TERMINATE) {
            shutdownInput.checkpointer.checkpoint()
        }
    }

    void onWorkerShutdown() {
        println("Worker shutdown.....")
        if (timerTask!=null) {
            timerTask.cancel()
        }
    }

    void onWorkerReady() {
        println("Worker ready......")
    }
}
