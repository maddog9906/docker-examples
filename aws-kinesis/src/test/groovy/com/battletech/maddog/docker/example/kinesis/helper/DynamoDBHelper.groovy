package com.battletech.maddog.docker.example.kinesis.helper

import com.amazonaws.ClientConfiguration
import com.amazonaws.Protocol
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder

class DynamoDBHelper {

    static AmazonDynamoDB getDynamoDBClient(String serviceEndpoint) {
        URL url = new URL(serviceEndpoint)
        if (!(url.protocol in ["http","https"])) {
            throw new RuntimeException("Only HTTPS or HTTP protocol is supported for DynamoDB!!!")
        }
        AmazonDynamoDBClientBuilder.standard()
                .withCredentials(AWSHelper.AWS_DUMMY_CREDENTIALS)
                .withClientConfiguration(new ClientConfiguration(protocol: url.protocol=="https"? Protocol.HTTPS : Protocol.HTTP))
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration("${url.host}:${url.port}", AWSHelper.AWS_REGION)
                ).build()
    }
}
