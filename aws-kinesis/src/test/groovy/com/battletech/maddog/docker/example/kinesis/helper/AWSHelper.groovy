package com.battletech.maddog.docker.example.kinesis.helper

import com.amazonaws.SDKGlobalConfiguration
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials

class AWSHelper {
    static final AWSCredentialsProvider AWS_DUMMY_CREDENTIALS =
            new AWSStaticCredentialsProvider(new BasicAWSCredentials("dummy","dummy"))
    static final AWS_REGION = "eu-west-2"

    static {
        System.setProperty(SDKGlobalConfiguration.AWS_CBOR_DISABLE_SYSTEM_PROPERTY, "true")
        System.setProperty(SDKGlobalConfiguration.DISABLE_CERT_CHECKING_SYSTEM_PROPERTY, "true")
    }
}
