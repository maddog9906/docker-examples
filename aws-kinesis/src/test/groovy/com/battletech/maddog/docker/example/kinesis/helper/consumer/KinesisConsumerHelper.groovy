package com.battletech.maddog.docker.example.kinesis.helper.consumer

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.kinesis.AmazonKinesis
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.WorkerStateChangeListener
import com.amazonaws.services.kinesis.metrics.interfaces.MetricsLevel
import com.battletech.maddog.docker.example.kinesis.helper.AWSHelper

class KinesisConsumerHelper {

    static Worker getWorker(NopRecordProcessor dummyRecordProcessor,
                            AmazonDynamoDB amazonDynamoDB,
                            AmazonKinesis amazonKinesis) {

         Worker worker = new Worker.Builder()
            .config(getClientConfig(dummyRecordProcessor.applicationName, dummyRecordProcessor.streamName))
            .recordProcessorFactory(new IRecordProcessorFactory() {
                @Override
                IRecordProcessor createProcessor() {
                    dummyRecordProcessor
                }
            })
            .workerStateChangeListener(new WorkerStateChangeListener() {
                @Override
                void onWorkerStateChange(WorkerStateChangeListener.WorkerState newState) {
                    switch (newState) {
                        case WorkerStateChangeListener.WorkerState.STARTED:
                            dummyRecordProcessor.onWorkerReady()
                            break
                        case WorkerStateChangeListener.WorkerState.SHUT_DOWN:
                            dummyRecordProcessor.onWorkerShutdown()
                            break
                    }
                }
            })
            .dynamoDBClient(amazonDynamoDB)
            .kinesisClient(amazonKinesis)
            .build()

        dummyRecordProcessor.shutdownClosure = {
            worker.shutdown()
        }

        worker

    }

    static private KinesisClientLibConfiguration getClientConfig(String applicationName, String streamName) {

        String workerId = InetAddress.getLocalHost().canonicalHostName + ":" + UUID.randomUUID()

        return new KinesisClientLibConfiguration(applicationName, streamName,
                AWSHelper.AWS_DUMMY_CREDENTIALS,
                workerId)
                .withInitialPositionInStream(InitialPositionInStream.TRIM_HORIZON)
                .withRegionName(AWSHelper.AWS_REGION)
                .withMetricsLevel(MetricsLevel.NONE)
    }
}
