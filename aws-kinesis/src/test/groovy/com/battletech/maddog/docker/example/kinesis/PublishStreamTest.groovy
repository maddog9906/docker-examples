package com.battletech.maddog.docker.example.kinesis

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.kinesis.AmazonKinesis
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker
import com.amazonaws.services.kinesis.model.*
import com.amazonaws.services.kinesis.producer.KinesisProducer
import com.amazonaws.services.kinesis.producer.UserRecordResult
import com.battletech.maddog.docker.example.kinesis.helper.DynamoDBHelper
import com.battletech.maddog.docker.example.kinesis.helper.KinesisHelper
import com.battletech.maddog.docker.example.kinesis.helper.KinesisProducerHelper
import com.battletech.maddog.docker.example.kinesis.helper.consumer.NopRecordProcessor
import com.battletech.maddog.docker.example.kinesis.helper.consumer.KinesisConsumerHelper
import groovy.json.JsonOutput
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll

import java.nio.ByteBuffer
import java.util.concurrent.Future

@Stepwise
class PublishStreamTest extends Specification {
    static String kinesisEndpoint = "http://localhost:4567"
    static String kinesisHttpsEndpoint = "https://localhost:8443"
    static String dynamoEndpoint = "http://localhost:8100"
    static String partitionKey = "PARTITION_KEY"
    static String streamName = "KINESIS_STREAM_TEST"

    KinesisProducer producer
    AmazonKinesis client
    AmazonDynamoDB dynamoDB

    @Shared
    String applicationName

    def setupSpec() {
        applicationName = "KCL_APP_${UUID.randomUUID()}"
    }

    def setup() {
        client = KinesisHelper.getKinesisClient(kinesisEndpoint)
        producer = KinesisProducerHelper.getKinesisProducer(kinesisHttpsEndpoint)
    }

    def "Create Stream"() {
        when:
        if (!KinesisHelper.isStreamAvailable(client, streamName)) {
            println("Creating stream $streamName ....")
            CreateStreamRequest createStreamRequest = new CreateStreamRequest()
            createStreamRequest.setStreamName(streamName)
            createStreamRequest.setShardCount(1)
            client.createStream(createStreamRequest)
        }
        KinesisHelper.waitForStream(client, streamName)

        then:
        assert true
    }

    def "Publish Streams using KPL"() {
        given:
        List<Future<UserRecordResult>> futures = []

        when:
        (0..99).each {
            byte[] data = JsonOutput.toJson([source:"KPL", uuid: UUID.randomUUID().toString(), idx: it]).getBytes("UTF-8")
            futures << producer.addUserRecord(streamName, partitionKey, ByteBuffer.wrap(data))
        }

        then:
        futures.each {
            UserRecordResult result = it.get()
            assert result.successful
            println "Put record into shard ${result.shardId}"
        }
    }

    def "Publish Streams using SDK"() {
        given:
        PutRecordsRequest putRecordsRequest  = new PutRecordsRequest(streamName: streamName)
        List <PutRecordsRequestEntry> putRecordsRequestEntryList =
                (100..149).collect {
                    PutRecordsRequestEntry putRecordsRequestEntry  = new PutRecordsRequestEntry()
                    byte[] data = JsonOutput.toJson([source: "SDK", uuid: UUID.randomUUID().toString(), idx: it]).getBytes("UTF-8")
                    putRecordsRequestEntry.setData(ByteBuffer.wrap(data))
                    putRecordsRequestEntry.setPartitionKey(partitionKey)
                    putRecordsRequestEntry
                }
        putRecordsRequest.setRecords(putRecordsRequestEntryList)

        when:
        PutRecordsResult putRecordsResult  = client.putRecords(putRecordsRequest)

        then:
        assert putRecordsResult.getFailedRecordCount() == 0

    }

    def "Consume Streams using SDK"() {
        given:
        DescribeStreamRequest describeRequest = new DescribeStreamRequest(streamName: streamName)
        DescribeStreamResult describedResultClient =  client.describeStream(describeRequest)
        Shard shard = describedResultClient.getStreamDescription().getShards()[0]
        GetShardIteratorRequest iteratorRequest =
                new GetShardIteratorRequest(streamName: streamName, shardId: shard.shardId, shardIteratorType: ShardIteratorType.TRIM_HORIZON)

        when:
        GetShardIteratorResult iteratorResponse = client.getShardIterator(iteratorRequest)

        and:
        String iteratorId = iteratorResponse.getShardIterator()
        GetRecordsRequest getRequest = new GetRecordsRequest(limit: 1000, shardIterator: iteratorId);
        GetRecordsResult getResponse = client.getRecords(getRequest)
        List<Record> records = getResponse.records
        if (records) {
            println("Received ${records.size()}")
        }
        getResponse.records.each { record ->
            String message = new String(record.getData().array(), "UTF-8")
            println("Message received = " + message)

        }

        then:
        assert true
    }

    @Unroll
    def "Consume Streams using KCL"() {
        given:
        dynamoDB = DynamoDBHelper.getDynamoDBClient(dynamoEndpoint)
        NopRecordProcessor processor = new NopRecordProcessor(applicationName, streamName, expectedRecordsCount)
        Worker worker = KinesisConsumerHelper.getWorker(processor, dynamoDB, client)

        when:
        worker.run()

        then:
        assert processor.recordsCount == expectedRecordsCount

        where:
        expectedRecordsCount << [150,0]

    }

    def "Delete Stream"() {
        when:
        DeleteStreamRequest deleteStreamRequest = new DeleteStreamRequest(streamName: streamName)
        client.deleteStream(deleteStreamRequest)

        then:
        assert true
    }
}
