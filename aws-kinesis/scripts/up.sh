#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export DOCKER_COMPOSE="docker-compose --log-level ERROR"
export COMPOSE_FILE="${WORKSPACE}/docker-compose.yml"

echo "WORKSPACE is: $WORKSPACE"

function start_service {
    ${DOCKER_COMPOSE} -f ${COMPOSE_FILE} up -d "$1" || exit 1
}

start_service dynamodb-mock
${PROJECT_DIR}/scripts/./wait-for-it.sh tcp://dynamodb-mock:8000
start_service kinesis-mock
${PROJECT_DIR}/scripts/./wait-for-it.sh tcp://kinesis-mock:4567
start_service kinesis-mock-proxy
${PROJECT_DIR}/scripts/./wait-for-it.sh tcp://kinesis-mock-proxy:443