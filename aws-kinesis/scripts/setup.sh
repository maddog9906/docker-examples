#!/usr/bin/env bash

function create_network {
    docker network ls -q -f "name=$1" | grep -q . || docker network create $1
}

function create_volume {
    docker volume ls -q -f "name=$1" | grep -q . || docker volume create --name=$1
}

create_volume kinesis-data
create_volume dynamodb-data
create_network default-nw