# kong-api-gw

## What is `kong-api-gw`
`kong-api-gw` .....

## Some info about this 
* Run `scripts/setup.sh` to setup the necessary docker network and volumes
* Run `scripts/start-api-gw.sh` to start kong api gw.
    * http://kong-api-gw:8000 - API GW end point
    * http://kong-api-gw:8001 - API GW Admin end point
    * http://konga:1337 - Konga Dashboard url 
    * postgres (kong-db) is listening at port 5432
* Run `scripts/down.sh` to shutdown everything.
