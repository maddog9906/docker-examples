#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export DOCKER_COMPOSE="docker-compose --log-level ERROR"
export COMPOSE_FILE="${WORKSPACE}/docker-compose.yml"

echo "WORKSPACE is: $WORKSPACE"

function start_service {
    ${DOCKER_COMPOSE} -f ${COMPOSE_FILE} up -d "$1" || exit 1
}

function open_page {
    ${PROJECT_DIR}/scripts/./open-page.sh $1
}

start_service kong-db
${PROJECT_DIR}/scripts/./wait-for-it.sh tcp://kong-db:5432
start_service kong-api-gw
start_service konga
${PROJECT_DIR}/scripts/./wait-for-it.sh http://konga:1337
open_page http://localhost:1337
