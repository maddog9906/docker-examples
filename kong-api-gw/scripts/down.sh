#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export COMPOSE_FILE="${WORKSPACE}/docker-compose.yml"

echo "WORKSPACE is: $WORKSPACE"

docker-compose -f ${COMPOSE_FILE} down -v