# aws-sns

## What is `aws-sns`
`aws-sns` is an example on how you could setup a local instance of AWS SNS. 

Check out this article - Running AWS SNS, SQS in docker (https://medium.com/@chewcysg/running-a-local-instance-of-aws-sns-4df9893b2e59)

## Some info about this 
* The `SNS` configuration is located at `config/db.json`
* The `SQS` configuration is located at `config/sqs_config/elasticmq.conf`
* The `sns proxy` is needed for HTTPS endpoints. 
The configuration is located at `config/ssl_proxy/proxy_ssl.conf`
* The `rabbitmq` is running with default settings. (Credentials: guest/guest)
* `Spock` test is also included to demonstrate publishing to SNS and 
receiving at SQS and Rabbitmq.

