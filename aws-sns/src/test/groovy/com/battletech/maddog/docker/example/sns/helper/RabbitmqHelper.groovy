package com.battletech.maddog.docker.example.sns.helper

import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Channel
import com.rabbitmq.client.GetResponse
import groovy.json.JsonSlurper

class RabbitmqHelper {

    static setupAMQP(String hostname, int port=5672) {
        ConnectionFactory factory = new ConnectionFactory(host: hostname, port: port)
        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()
        [connection: connection, channel: channel]
    }

    static String receiveMessage(Channel channel, String queueName, boolean autoAck=true) {
        GetResponse response = channel.basicGet(queueName, autoAck)
        if (response) {
            def messageBody = new JsonSlurper().parseText(new String(response.body, "UTF-8"))
            messageBody.'Message' as String
        } else {
            null
        }
    }

    static void cleanupAMQP(def rabbitmq) {
        if (rabbitmq && rabbitmq.channel) {
            Channel channel = rabbitmq.channel as Channel
            channel.close()
        }

        if (rabbitmq && rabbitmq.connection) {
            Connection connection = rabbitmq.connection as Connection
            connection.close()
        }
    }
}
