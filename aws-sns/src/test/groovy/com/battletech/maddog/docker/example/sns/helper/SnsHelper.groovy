package com.battletech.maddog.docker.example.sns.helper

import com.amazonaws.ClientConfiguration
import com.amazonaws.Protocol
import com.amazonaws.SDKGlobalConfiguration
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.AmazonSNSClientBuilder
import com.amazonaws.services.sns.model.PublishRequest
import com.amazonaws.services.sns.model.PublishResult
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.PurgeQueueRequest
import com.amazonaws.services.sqs.model.PurgeQueueResult
import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import groovy.json.JsonSlurper

class SnsHelper {

    static final AWSCredentialsProvider AWS_DUMMY_CREDENTIALS =
            new AWSStaticCredentialsProvider(new BasicAWSCredentials("dummy","dummy"))

    static AmazonSNS getSns(String serviceEndpoint, String region, boolean ssl) {
        if (ssl) {
            System.setProperty(SDKGlobalConfiguration.DISABLE_CERT_CHECKING_SYSTEM_PROPERTY, "true");
        }
        AmazonSNSClientBuilder.standard()
                .withClientConfiguration(new ClientConfiguration(protocol: ssl? Protocol.HTTPS : Protocol.HTTP))
                .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration(serviceEndpoint, region))
                .withCredentials(AWS_DUMMY_CREDENTIALS)
                .build()
    }

    static AmazonSQS getSqs(String serviceEndpoint, String region, boolean ssl) {
        if (ssl) {
            System.setProperty(SDKGlobalConfiguration.DISABLE_CERT_CHECKING_SYSTEM_PROPERTY, "true");
        }
        AmazonSQSClientBuilder.standard()
                .withCredentials(AWS_DUMMY_CREDENTIALS)
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(serviceEndpoint, region))
                .withClientConfiguration(new ClientConfiguration(protocol: ssl? Protocol.HTTPS : Protocol.HTTP))
                .build()
    }

    static PublishResult publishMessage(AmazonSNS sns, String topicArn, String message) {
        PublishRequest publishRequest = new PublishRequest(topicArn, message)
        sns.publish(publishRequest)
    }

    static PurgeQueueResult purgeQueue(AmazonSQS sqs, String queueUrl) {
        PurgeQueueRequest req = new PurgeQueueRequest(queueUrl)
        sqs.purgeQueue(req)
    }

    static List<String> receiveMessages(AmazonSQS sqs, String queueUrl, int maxNumberOfMessages=10, int waitTimeSeconds=5) {
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl)
                .withWaitTimeSeconds(waitTimeSeconds)
                .withMaxNumberOfMessages(maxNumberOfMessages)

        sqs.receiveMessage(receiveMessageRequest).getMessages().collect {
            def messageBody = new JsonSlurper().parseText(it.body)
            messageBody.'Message' as String
        }
    }

}
