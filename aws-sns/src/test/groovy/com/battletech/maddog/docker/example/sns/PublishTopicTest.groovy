package com.battletech.maddog.docker.example.sns

import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sqs.AmazonSQS
import com.battletech.maddog.docker.example.sns.helper.RabbitmqHelper
import com.battletech.maddog.docker.example.sns.helper.SnsHelper
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors

class PublishTopicTest extends Specification {
    static String awsZone = "eu-west-2"
    static String sqsEndpoint = "localhost:9324"
    static String sqsBaseUrl = "http://$sqsEndpoint/queue"

    AmazonSQS amazonSQS

    def setup() {
        amazonSQS = SnsHelper.getSqs(sqsEndpoint,awsZone, false)
        SnsHelper.purgeQueue(amazonSQS,"$sqsBaseUrl/test-topic")
        SnsHelper.purgeQueue(amazonSQS,"$sqsBaseUrl/test-fanout1")
        SnsHelper.purgeQueue(amazonSQS,"$sqsBaseUrl/test-fanout2")
    }

    @Unroll
    def "publish to SNS Topic and received in SQS Queue"() {
        given:
        String uuid = UUID.randomUUID().toString()
        String message = "Test Message - $uuid"
        AmazonSNS sns = SnsHelper.getSns(snsEndpoint, awsZone, ssl)

        when:
        SnsHelper.publishMessage(sns, "arn:aws:sns:eu-west-2:123450000001:test-topic", message)

        and:
        List<String> messages = SnsHelper.receiveMessages(amazonSQS, "$sqsBaseUrl/test-topic")

        then:
        assert messages.size() == 1
        assert messages[0] == message

        // purge the queue
        SnsHelper.purgeQueue(amazonSQS, "$sqsBaseUrl/test-topic")

        where:
        snsEndpoint         | ssl
        "localhost:8443"    | true
        "localhost:9911"    | false
    }

    def "publish to SNS Topic and fanout to 2 SQS Queues"() {
        given:
        String uuid = UUID.randomUUID().toString()
        String message = "Test Fanout Message - $uuid"
        AmazonSNS sns = SnsHelper.getSns("localhost:9911", awsZone, false)

        when:
        SnsHelper.publishMessage(sns, "arn:aws:sns:eu-west-2:123450000001:test-fanout", message)

        and:
        List<String> messages = ["test-fanout1","test-fanout2"].parallelStream()
                                    .map({
                                        List<String> messages = SnsHelper.receiveMessages(amazonSQS, "$sqsBaseUrl/$it")
                                        assert messages.size() == 1
                                        assert messages[0] == message
                                        messages[0]
                                    })
                                    .collect(Collectors.toList())

        then:
        assert messages.size() ==2

        // purge the queues
        SnsHelper.purgeQueue(amazonSQS, "$sqsBaseUrl/test-fanout1")
        SnsHelper.purgeQueue(amazonSQS,"$sqsBaseUrl/test-fanout2")
    }

    def "publish to SNS Topic and receive in rabbitmq"() {
        given:
        String uuid = UUID.randomUUID().toString()
        String message = "Test RabbitMQ Message - $uuid"
        AmazonSNS sns = SnsHelper.getSns("localhost:9911", awsZone, false)
        def rabbitmq = RabbitmqHelper.setupAMQP("localhost")

        when:
        SnsHelper.publishMessage(sns, "arn:aws:sns:eu-west-2:123450000001:test-rabbitmq", message)

        and:
        String messageReceived = RabbitmqHelper.receiveMessage(rabbitmq.channel, "amq-sns-queue")

        then:
        assert messageReceived == message

        cleanup:
        RabbitmqHelper.cleanupAMQP(rabbitmq)

    }
}
